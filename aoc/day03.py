# task1

"""
string for converting to alphabets
"""


import string

with open("./day03.txt") as data:
    data = data.read().strip().split("\n")


dict = {}
alpha = string.ascii_letters
count = 1
for x in range(0,len(alpha)):
    dict[alpha[x]] = count
    count += 1

common = []
for i in data:
    len_ = len(i)//2
    a = i[0:len_]
    b = i[len_:]
    c = [x for x in a if x in b]
    common.append(c[0])


count =0
for i in (common):
    count += dict[i]

print(f'task1: {count}')


# task2

common = []
for i in range(0,len(data)-1,3):
    a = data[i]
    b = data[i+1]
    c = data[i+2]
    d = [x for x in set(a) if x in b and x in c]

    common.append(d[0])


count =0
for i in (common):
    count += dict[i]

print(f'task2: {count}')
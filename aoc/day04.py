# task1

with open("./day04.txt") as data:
    data = data.read().strip().split("\n")

count = 0

for i in data:
    tmp = i.split(",")
    tmp1 = tmp[0].split('-')
    tmp2 = tmp[1].split('-')

    list1 = []
    for i in range(int(tmp1[0]), int(tmp1[1])+1):
        list1.append(i)
    list2 = []
    for i in range(int(tmp2[0]), int(tmp2[1])+1):
        list2.append(i)

    if (len(list1) >= len(list2)):
        if (set(list2).issubset(set(list1))):
            count += 1
        else:
            continue
    elif(len(list2) > len(list1)):
        if (set(list1).issubset(set(list2))):
            count += 1
        else:
            continue
    else:
        print("ERROR")
    

print(f'task1: {count}')


# task2


count = 0

for i in data:
    tmp = i.split(",")
    tmp1 = tmp[0].split('-')
    tmp2 = tmp[1].split('-')

    list1 = []
    for i in range(int(tmp1[0]), int(tmp1[1])+1):
        list1.append(i)

    list2 = []
    for i in range(int(tmp2[0]), int(tmp2[1])+1):
        list2.append(i)

    if (len(list1) >= len(list2)):
        if (any(x in list2 for x in list1)):
            count += 1
        else:
            continue
    elif(len(list2) > len(list1)):
        if (any(x in list1 for x in list2)):
            count += 1
        else:
            continue
    else:
        print("ERROR")
    

print(f'task2: {count}')
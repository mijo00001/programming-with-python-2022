# task1

with open("./day06.txt") as file:
    data = file.read()

count_ = 0
for i in range(len(data)+1):
    temp = data[i:i+4]
    temp1 = set(temp)
    if len(temp1) == 4:
        count_ = i+4
        break
    else:
        continue

print(f'task1: {count_}')


# task2


count_ = 0
for i in range(len(data)+1):
    temp = data[i:i+14]
    temp1 = set(temp)
    if len(temp1) == 14:
        count_ = i+14
        break
    else:
        continue
print(f'task2: {count_}')
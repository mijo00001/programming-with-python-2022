

"""
task1 takes about 15 sec to run

task2 take much much longer  32m36.304s

"""

# task1

with open("./day14.txt") as data:
    data = data.read().strip().split("\n")

cave = []


for line in data:
    line = line.split("->")
    px, py = 0, 0
    
    for i in line:
        x,y = map(int,i.split(","))
        cave.append((x,y))

        nx = x - px 
        ny = y - py

        
        if nx != 0 and abs(nx) != x :
            minx = min(px, x)
            for i in range(abs(nx)):
                bx = minx+i
                cave.append((bx, py))
        
        if ny != 0 and abs(ny) != y:
            miny = min(py, y)
            for i in range(abs(ny)):
                by = miny+i
                cave.append((px, by))
        px = x
        py = y



maxY = []
for x,y in set(cave):
    maxY.append(y)
maxY = max(maxY)



signal = ""
count = 0
while signal != "stop":
    sx = 500
    sy = 0    
    while sy < maxY:
        if (sx,sy+1) in cave:                       #down
            if (sx-1, sy+1) in cave:                #left
                if (sx+1, sy+1) in cave:            #right
                    cave.append((sx,sy))
                    count +=1
                    break
                else:
                    if sy+1 is not maxY:
                        sx, sy = sx+1, sy+1
              
                    else:
                        signal = "stop"
                        break   
            else:
                if sy+1 is not maxY:
                    sx, sy = sx-1, sy+1
                    
                else:
                    signal = "stop"
                    break
        else:
            if sy+1 is not maxY:
                sy = sy+1
            else:
                signal = "stop"
                break
    



print("task1: ",count)




# task2:


cave = []             

for line in data:
    line = line.split("->")
    px, py = 0, 0
    
    for i in line:
        x,y = map(int,i.split(","))
        cave.append((x,y))

        nx = x - px 
        ny = y - py

        
        if nx != 0 and abs(nx) != x :
            minx = min(px, x)
            for i in range(abs(nx)):
                bx = minx+i
                cave.append((bx, py))
        
        if ny != 0 and abs(ny) != y:
            miny = min(py, y)
            for i in range(abs(ny)):
                by = miny+i
                cave.append((px, by))
        px = x
        py = y



maxY = []
for x,y in set(cave):
    maxY.append(y)
maxY = max(maxY)
floor = maxY + 2



count = 0


while (500, 0) not in set(cave):

    sx = 500
    sy = 0    


    while sy < floor:
        if (sx,sy+1) in cave:                   #down
            
            if (sx-1, sy+1) in cave:              #left

                if (sx+1, sy+1) in cave:          #right
                    cave.append((sx,sy))
                    count +=1
                    break
                else:

                    if sy+1 is not floor:
                        sx, sy = sx+1, sy+1
                    
                    else:
                        cave.append((sx,sy))
                        count +=1
                        break   
            else:
                if sy+1 is not floor:
                    sx, sy = sx-1, sy+1
                    
                else:
                    cave.append((sx,sy))
                    count +=1
                    break
        else:
            if sy+1 is not floor:
                sy = sy+1
            else:
                cave.append((sx,sy))
                count +=1
                break

print("task2: ",count)



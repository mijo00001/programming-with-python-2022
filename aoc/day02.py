with open("./day02.txt") as data:
    data = data.read().strip().split("\n")


# task1

count = 0

for i in data:
    a = i.split(" ")[0]
    b = i.split(" ")[1]
    if a == "A":
        match b:
            case "X":
                count += 4
            case "Y":
                count += 8
            case "Z":
                count += 3
    elif a == "B":
        match b:
            case "X":
                count += 1
            case "Y":
                count += 5
            case "Z":
                count += 9
    elif a == "C":
        match b:
            case "X":
                count += 7
            case "Y":
                count += 2
            case "Z":
                count += 6
    else:
        print("wrong input")

print(f'task1: {count}')

# task2

count = 0

for i in data:
    a = i.split(" ")[0]
    b =i.split(" ")[1]

    match b:
        case "X":
            if a == "A":
                count += 3
            elif a == "B":
                count += 1
            elif a == "C":
                count += 2
            else:
                print("wrong input")
        case "Y":
            if a == "A":
                count += 4
            elif a == "B":
                count += 5
            elif a == "C":
                count += 6
            else:
                print("wrong input")
        case "Z":
            if a == "A":
                count += 8
            elif a == "B":
                count += 9
            elif a == "C":
                count += 7
            else:
                print("wrong input")

print(f'task2: {count}')

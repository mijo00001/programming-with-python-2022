with open("./day07.txt") as file:
    data = file.readlines()

dirs = {"/home":0}
current = "/home"

# Process every line
for entry in data:
    line = entry.strip("\n")
    line = line.split(" ")

    if line[0] == '$':

        if line[1] == "cd":

            if line[2] == "/":
                  current = "/home"                
            elif line[2] == "..":
                if current != "/home":
                    current = current[:current.rfind("/")]
            else: 
                current = current + "/" + line[2]
                if current not in dirs.keys():
                    dirs[current] = 0    

        elif line[1] == "ls":
            pass

    elif line[0] == "dir":
        pass

    elif line[0].isnumeric():
        # add vales to key to all dir in path
        tmpPath = current
        for dir in range(tmpPath.count("/")):
            if tmpPath in dirs.keys():
                dirs[tmpPath] += int(line[0])
                tmpPath = tmpPath[:tmpPath.rfind("/")]


count = 0

for key in dirs:
    if dirs[key] < 100000:
        count += dirs[key]

print(f'task1: {count}')



# task2


toFree = 30000000 - (70000000 - dirs["/home"])           #the required free space
toDel = []

for key in dirs:
    if dirs[key] >= toFree:
        toDel.append(dirs[key])

print(f'task2: {min(toDel)}')
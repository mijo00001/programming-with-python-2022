"""
adapted line 25 from nitekat1124 (https://github.com/nitekat1124/advent-of-code-2022/blob/main/solutions/day20.py) because the conditions when written for forward and reverse separately didn't yield the correct answer.

I adapted code in line 33 from the same git because it looks nice. Some friends helped to clean up the code
"""


with open("./day20.txt") as data:
    data = data.read().strip().split("\n")

encrypt = []

for id, val in enumerate(data):
    encrypt.append((id, int(val)))

orig_input = encrypt.copy()

length = len(encrypt)


for rank, value in orig_input:

        pos = encrypt.index((rank, value))
        move = (pos + value + (length - 1)) % (length - 1)
        encrypt.insert(move, encrypt.pop(pos))


zero_pos = encrypt.index((data.index(str(0)), 0))
result = encrypt[(zero_pos + 1000) % length][1] + encrypt[(zero_pos + 2000) % length][1] + encrypt[(zero_pos + 3000) % length][1]

print(f'task1: {result}')
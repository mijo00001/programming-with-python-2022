"""

task1: the formula to calculate area: idea from Feng https://www.youtube.com/watch?v=dz5Sx9sUfRA&list=PLsqh-jhhTL2-cJiIRlju1sQj0i_Sk9zmd&index=3&ab_channel=WilliamY.Feng


Task2 takes a long time: 38m20.535s

"""


with open("./day23.txt") as data:
    data = data.read().strip().split("\n")

directions = ["N", "S", "W", "E"]

dirCheck = {"N": [(-1, -1), (-1, 0), (-1, 1)],
            "S": [(1, -1), (1, 0), (1, 1)],            
            "W": [(1, -1), (0, -1), (-1, -1)],
            "E": [(1, 1), (0, 1), (-1, 1)]}

grid = []

for i in data:
    temp = []
    for j in i:
        temp.append(j)
    grid.append(temp)

elves = []
for r, row in enumerate(grid):
    for c, val in enumerate(row):
        if val == "#" :
            elves.append((r, c))


def Current(elves, elf , current):
    er, ec = elf
    for d in current:
        for r,c in dirCheck[d]:
            if (er + r, ec + c) not in elves:
                return [elf, d]
            else:
                continue
    return[]
                
for a in range(10):

    current = []

    current.append(directions[a %len(directions)])
    current.append(directions[(a+1) %len(directions)])
    current.append(directions[(a+2) %len(directions)])
    current.append(directions[(a+3) %len(directions)])

    proposed = []

    for elf in elves:
        er, ec = elf
        count = 0
    
        temp = {}
        for d in current:
            count2 = 0
            for r,c in dirCheck[d]:
                if (er +r, ec+c) in elves:
                    count +=1
                else:
                    count2 +=1
                    temp[d] = count2                

        if count == 0:
            continue

        else:
            for key in temp:
                if temp[key] == 3:
                    proposed.append((elf, key))
                    break

    news = {"N" : (-1,0), "S": (1,0), "W": (0, -1), "E": (0, 1)}
    updated = {}
    update = []

    # for all the proposed move:
    for i in proposed:

        er, ec = i[0]
        move = i[1]

        r, c = news[move]

        updated[(er+r, ec+c)] =  (er, ec)
        update.append((er+r, ec+c))

    my_list = [x for x in update if update.count(x) > 1]

    for i in set(my_list):
        del updated[i]

    for new in updated:
        elves.append(new)
        elves.remove(updated[new])


# take min/ max values
r, c = [], []
for i in elves:
    r.append(i[0])
    c.append(i[1])

maxC, minC = max(c), min(c)
maxR, minR = max(r), min(r)


print(f' task1: {(maxR - minR + 1) * (maxC - minC + 1) - len(elves)}')             #idea from Feng



# task2

a =0
while True:
    current = []

    current.append(directions[a %len(directions)])
    current.append(directions[(a+1) %len(directions)])
    current.append(directions[(a+2) %len(directions)])
    current.append(directions[(a+3) %len(directions)])

    proposed = []

    for elf in elves:
        er, ec = elf
        count = 0
    
        temp = {}
        for d in current:
            count2 = 0
            for r,c in dirCheck[d]:
                if (er +r, ec+c) in elves:
                    count +=1
                else:
                    count2 +=1
                    temp[d] = count2                

        if count == 0:
            continue

        else:
            for key in temp:
                if temp[key] == 3:
                    proposed.append((elf, key))
                    break


    if len(proposed) == 0:
        break

    news = {"N" : (-1,0), "S": (1,0), "W": (0, -1), "E": (0, 1)}
    updated = {}
    update = []

    # for all the proposed move:
    for i in proposed:

        er, ec = i[0]
        move = i[1]

        r, c = news[move]

        updated[(er+r, ec+c)] =  (er, ec)
        update.append((er+r, ec+c))

    my_list = [x for x in update if update.count(x) > 1]

    for i in set(my_list):
        del updated[i]

    for new in updated:
        elves.append(new)
        elves.remove(updated[new])

    a+=1


print(f' task 2: {a+1}')
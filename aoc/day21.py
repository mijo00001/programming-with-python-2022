"""
for task 2, I used sympy* package (https://www.sympy.org/en/index.html) which pretty much carried out calculations without a variable's value in numeric. it saves the compuations for later calculation and then with "solve" solves the equation. here we want to see if the two alues in root are the same. To find that we could solve by subtracting to zero. 

*Sympy:
Meurer A, Smith CP, Paprocki M, Čertík O, Kirpichev SB, Rocklin M, Kumar A,
Ivanov S, Moore JK, Singh S, Rathnayake T, Vig S, Granger BE, Muller RP,
Bonazzi F, Gupta H, Vats S, Johansson F, Pedregosa F, Curry MJ, Terrel AR,
Roučka Š, Saboo A, Fernando I, Kulal S, Cimrman R, Scopatz A. (2017) SymPy:
symbolic computing in Python. *PeerJ Computer Science* 3:e103
https://doi.org/10.7717/peerj-cs.103


Thanks to hyper-neutrino for explaining how sympy works (https://www.youtube.com/watch?v=FeH1fE6wLUI&list=PLnNm9syGLD3yf-YW-a5XNh1CJN07xr0Kz&index=21&t=617s&ab_channel=hyper-neutrino)
"""


with open("./day21.txt") as data:
    data = data.read().strip().split("\n")
    
withVal = {}
withoutVal = {}

for i in data:
    if len(i) < 10:
        i = i.split(": ")
        withVal[i[0]] = int(i[1])
    elif len(i) > 10:
        i = i.split(": ")
        withoutVal[i[0]] = i[1].split(" ")


def without(withoutVal):
    key = []
    for keys in withoutVal:
        temp = withoutVal[keys]

        if temp[0] in withVal.keys() and  temp[2] in withVal.keys():

            if temp[1] == "+":
                withVal[keys] = withVal[temp[0]] + withVal[temp[2]]
            elif temp[1] == "-":
                withVal[keys] = withVal[temp[0]] - withVal[temp[2]]
            elif temp[1] == "*":
                withVal[keys] = withVal[temp[0]] * withVal[temp[2]]
            elif temp[1] == "/":
                withVal[keys] = withVal[temp[0]] // withVal[temp[2]]
            key.append(keys)
        else:
            continue
    
    for i in key:
        del withoutVal[i]

    return withoutVal



while withoutVal:
    withoutVal = without(withoutVal)


print(f'task1: {withVal["root"]}')


# task2


import sympy

with open("./day21.txt") as data:
    data = data.read().strip().split("\n")

    
withVal = {"humn" : sympy.Symbol("x")}

withoutVal = {}


for i in data:
    if len(i) < 10:
        i = i.split(": ")
        if i[0] in withVal.keys():
            continue
        else:
            withVal[i[0]] = sympy.Integer(i[1])
    elif len(i) >= 10:
        i = i.split(": ")
        withoutVal[i[0]] = i[1].split(" ")


def without(withoutVal):
    key = []
    for keys in withoutVal:
        temp = withoutVal[keys]
        if temp[0] in withVal.keys() and  temp[2] in withVal.keys():
            left = withVal[temp[0]]
            right = withVal[temp[2]]
        
            if temp[1] == "+":
                withVal[keys] = left + right
            elif temp[1] == "-":
                withVal[keys] = left - right
            elif temp[1] == "*":
                withVal[keys] = left * right
            elif temp[1] == "/":
                withVal[keys] = left / right
            key.append(keys)
        else:
            continue

    for i in key:
        del withoutVal[i]


    if len(withoutVal.keys()) !=1:
        withoutVal = without(withoutVal)

    return withoutVal




withoutVal = without(withoutVal)


root = withoutVal["root"]
result = sympy.solve(withVal[root[0]] - withVal[root[2]])

print(f'task2: {result}')

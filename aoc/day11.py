"""
I imported math for calculating floor of numbers

Task2:
    -Modulo idea from William Y. Feng (https://www.youtube.com/watch?v=63-uEScYUvM&t=180s&ab_channel=WilliamY.Feng )


"""

import math             #for floor



with open("./day11.txt") as data:
    data = data.read().strip()
    blocks = data.split("\n\n")


to_add = {}
inspection ={}

def operation_(item, symbol, value):
    if symbol == "+":
        if value == "old":
            answer = int(item) + int(item)
        else:
            answer = int(item) + int(value)
    elif symbol == "*":
        if value == "old":
            answer = int(item) * int(item)
        else:
            answer = int(item) * int(value)
    return answer


for rounds in range(20):
    for lines in blocks:
        line = lines.strip()
        line = line.split("\n")

        # money number
        monkey =  line[0].strip()
        monkey = monkey.split(" ")[1].strip(":")
        
        # split by : for worry
        if rounds > 0:
            worry = to_add[monkey]
        else:
            worry = line[1].split(":")[1].split(",")

            if monkey in to_add.keys():
                worry =  worry + to_add[monkey]
        to_add[monkey] = []

        # split by " " to get operation
        operation =  line[2].strip()
        operation = operation.split(" ")[4:]

        # split by " " to get test
        test =  line[3].strip()
        test = test.split(" ")[-1]

        # split by " " to get True
        true_ =  line[4].strip()
        true_ = true_.split(" ")[-1]

        #  split by " " to get True
        false_ =  line[5].strip()
        false_ = false_.split(" ")[-1]


        for item in worry:
            op = operation_(item, operation[0], operation[1])
            reduced = math.floor(op/3)
            if reduced%int(test) == 0:
                if true_ not in to_add.keys():
                    to_add[true_] = []
                to_add[true_].append(reduced)
            else:
                if false_ not in to_add.keys():
                    to_add[false_] = []
                to_add[false_].append(reduced)

        if monkey not in inspection.keys():
            inspection[monkey] = 0
        inspection[monkey] += len(worry)
    

most_active = sorted(inspection, key=inspection.get, reverse=True)[:2]
answer = 1

for value in most_active:
    answer *= int(inspection[value])

print(f'task1: {answer}')


# task2

with open("./day11.txt") as data:
    data = data.read().strip()
    blocks = data.split("\n\n")



to_add = {}
inspection ={}

modulo = 1

for lines in blocks:
    line = lines.strip()
    line = line.split("\n")


    # split by " " to get test
    t =  line[3].strip()
    t = t.split(" ")[-1]
    modulo *= int(t)


def operation_(item, symbol, value):
    if symbol == "+":
        if value == "old":
            answer = int(item) + int(item)
        else:
            answer = int(item) + int(value)
    elif symbol == "*":
        if value == "old":
            answer = int(item) * int(item)
        else:
            answer = int(item) * int(value)

    return answer % modulo


for rounds in range(10000):
    for lines in blocks:
        line = lines.strip()
        line = line.split("\n")

        # money number
        monkey =  line[0].strip()
        monkey = monkey.split(" ")[1].strip(":")
        
        # split by : for worry
        if rounds > 0:
            worry = to_add[monkey]
        else:
            worry = line[1].split(":")[1].split(",")

            if monkey in to_add.keys():
                worry =  worry + to_add[monkey]
        to_add[monkey] = []

        # split by " " to get operation
        operation =  line[2].strip()
        operation = operation.split(" ")[4:]

         # split by " " to get test
        test =  line[3].strip()
        test = test.split(" ")[-1]

        # split by " " to get True
        true_ =  line[4].strip()
        true_ = true_.split(" ")[-1]

        #  split by " " to get True
        false_ =  line[5].strip()
        false_ = false_.split(" ")[-1]


        for item in worry:
            op = operation_(item, operation[0], operation[1])

            if op % int(test) == 0:
                if true_ not in to_add.keys():
                    to_add[true_] = []
                to_add[true_].append(op)
            else:
                if false_ not in to_add.keys():
                    to_add[false_] = []
                to_add[false_].append(op)

        if monkey not in inspection.keys():
            inspection[monkey] = 0
        inspection[monkey] += len(worry)
    
most_active = sorted(inspection, key=inspection.get, reverse=True)[:2]
answer = 1

for value in most_active:
    answer *= int(inspection[value])

print(f'task2: {answer}')
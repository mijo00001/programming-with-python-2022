"""

since the adjacent faces would be different from the current by 1 in all 6 directions, a condt would suffice

task2: takes a long time to compute: 5m40.264s. 


Videos by Feng, hyper neutrino, and 0xdf were helpful to understand the second part

https://www.youtube.com/watch?v=7tlWvZTPz1c&t=413s&ab_channel=hyper-neutrino
https://www.youtube.com/watch?v=xPIMCrbRPOI&t=274s&ab_channel=WilliamY.Feng
https://www.youtube.com/watch?v=LC62vsvPjZc&list=PLJt6nPUdQbiTXW_lHJ6d2u8NnsyPaKGeI&index=21&ab_channel=0xdf



"""


cubes = []
count = 0

with open("./day18.txt") as data:
    data = data.read().strip().split("\n")
    for i in data:
        x,y,z = list(map(int, i.split(",")))
        cubes.append((x,y,z))



for (x,y,z) in cubes:
    if  (x+1,y,z) not in cubes:
        count+=1 
    if  (x,y+1,z) not in cubes:
        count+=1 
    if  (x,y,z+1) not in cubes:
        count+=1 
    if  (x-1,y,z) not in cubes:
        count+=1 
    if  (x,y-1,z) not in cubes:
        count+=1 
    if  (x,y,z-1) not in cubes:
        count+=1 


print(f'task1: {count}')



# task2


cubes = []
count = 0

with open("./day18.txt") as data:
    data = data.read().strip().split("\n")
    for i in data:
        x,y,z = list(map(int, i.split(",")))
        cubes.append((x,y,z))

x, y, z = [], [], []

for a, b, c in cubes:
    x.append(a)
    y.append(b)
    z.append(c)

maxX, minX = max(x), min(x)
maxY, minY = max(y), min(y)
maxZ, minZ = max(z), min(z)

def outside(x, y, z):

    queue = []
    traversed = []
    queue.append((x, y, z))
    while queue:
        x, y, z = queue.pop(0)
        if (x, y, z) in traversed:
            continue
        else: 
            traversed.append((x, y, z))
            if (x, y, z) in cubes:
                continue
            else:
                if x > maxX or x < minX or y > maxY or y < minY or z > maxZ or z < minZ:
                    return 1
        for dx, dy, dz in adjacent:  
            queue.append((x + dx, y + dy, z + dz))
    return 0
        



adjacent = [(1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1)]


for (x,y,z) in cubes:
    for (nx, ny, nz) in adjacent:
        count += outside(x + nx, y + ny, z + nz)
        

print(f'task2: {count}')

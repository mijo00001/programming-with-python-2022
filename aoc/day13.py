"""

part2:
I attempted flattening the lists and sorting them but unlike the toy data it didn't do well with the actual input. I adapted my code based on the logic by hyper-nutrino (https://www.youtube.com/watch?v=k6WOnlpiYBQ&ab_channel=hyper-neutrino) 
"""


with open("./day13.txt") as data:
    data = data.read().strip().split("\n\n")

input = []

for i in data:
    left, right = i.split("\n")
    input.append((left, right))

def func(left, right):

    # for both int:
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        elif left == right:
            pass
        elif left > right:
            return 0


    # for diff types:
    if type(left) is not type(right):
        if isinstance(left, int):
            x = func([left], right)
        elif isinstance(right, int):
            x = func(left, [right])
        if x ==1:
            return 1
        elif x ==0:
            return 0

    # for both lists:
    if isinstance(left, list) and isinstance(right, list):
        for (l,r) in zip(left, right):
            x = func(l, r)
            if x ==1:
                return 1
            elif x ==0:
                return 0


        if len(left) < len(right):
            return 1
        if len(left) > len(right):
            return 0



count = []
for (a, b) in input:
    count.append(func(eval(a),eval(b)))

sum_ = 0
for i in range(len(count)):
    if count[i] == 1:
        sum_ += i+1
    

print(f'task1: {sum_}')




"""
I attempted flattening the lists and sorting them but unlike the toy data it didn't do well with the actual input. I adapted my code based on the logic ny hyper-nutrino (https://www.youtube.com/watch?v=k6WOnlpiYBQ&ab_channel=hyper-neutrino) 
"""
with open("./day13.txt") as data:
    data = data.read().strip().split("\n\n")

input = []

for i in data:
    left, right = i.split("\n")
    input.append(eval(left))
    input.append(eval(right))



def func(left, right):



    # for both int:
    if isinstance(left, int) and isinstance(right, int):
        if left < right:
            return 1
        elif left == right:
            pass
        elif left > right:
            return 0


    # for diff types:
    if type(left) is not type(right):
        if isinstance(left, int):
            x = func([left], right)
        elif isinstance(right, int):
            x = func(left, [right])
        if x ==1:
            return 1
        elif x ==0:
            return 0

    # for both lists:
    if isinstance(left, list) and isinstance(right, list):
        for (l,r) in zip(left, right):
            x = func(l, r)
            if x ==1:
                return 1
            elif x ==0:
                return 0

        if len(left) < len(right):
            return 1
        if len(left) > len(right):
            return 0


# including the indices of [[2]] and [[6]]
i2 = [1]
i6 = [1,1]


for a in input:
    i2.append(func(a,[[2]]))
    i6.append(func(a,[[6]]))

print(f'task1: {sum(i2) * sum(i6)}')
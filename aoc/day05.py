#task1 

data1 = ""

with open("./day05.txt") as data:
    data = data.read().strip("\n")

# split the stack part
for i in data.split("\n"):
    if i == "":
        break
    else:
        data1 = data1 + "\n" + i

crate_no = int(data1[-2])

crate = [] 
for i in reversed(data1.split("\n")[1:-1]):
    temp = []
    for j in range(1,len(i),4):
        temp.append(i[j])
    crate.append(temp)

crate_ = []
for i in range(crate_no):
    temp = []
    for j in crate:
        if j[i] != ' ':
            temp.append(j[i])  
        else:
            continue
    crate_.append(temp)


command_start = (data1.count("\n")) + 1

command = ""
for i in data.split("\n")[command_start:]:
    i = i.split(" ")

    move_ = int(i[1])
    from_ = int(i[3]) - 1
    to_   = int(i[5]) - 1

    popped = []
    for i in range(int(move_)):
        popped.append(crate_[from_].pop())
    crate_[to_].extend(popped)




result = ""
for i in crate_:
    result = result + i[-1]
print(f'task1: {result}')


#-------------------------------------------------------------

#task2



crate_ = []
for i in range(crate_no):
    temp = []
    for j in crate:
        if j[i] != ' ':
            temp.append(j[i])  
        else:
            continue
    crate_.append(temp)


# split the command part and add crates

command_start = (data1.count("\n")) + 1



command =""
for i in data.split("\n")[command_start:]:
    i = i.split(" ")
    move_ = int(i[1])
    from_ = int(i[3])-1
    to_ = int(i[5])-1

    popped=[]
    for i in range(int(move_)):
        popped.append(crate_[from_].pop())
    crate_[to_].extend(list(reversed(popped)))



result = ""

for i in crate_:
    result = result + i[-1]

print(f'task2: {result}')
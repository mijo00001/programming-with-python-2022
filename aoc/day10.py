"""
line 94:  #better readability code that removed strings. source: https://www.geeksforgeeks.org/python-ways-to-print-list-without-quotes/
"""

with open("./day10.txt") as file:
    data = file.read().strip()
    data = data.split("\n")


sigStrength = [20, 60, 100, 140, 180, 220]
cycle = 0         
x = 1
i=0
SigX = []

for i in range(len(data)):
    if cycle > 221:
        break
    line = data[i].split(" ") 

    if line[0] == "noop":
        cycle += 1

        if cycle in sigStrength: 
            SigX.append(cycle * x)

    elif line[0] == "addx":
        value = line[1]
        for j in range(2):
            cycle += 1
            
            if cycle in sigStrength: 
                SigX.append(cycle * x)

        x=  x + int(value)
    
    
print(f'task1: {sum(SigX)}')




# task2

cycle = 0 
middle = 1
result = []

def testOverlap(cycle, middle):
    middle = int(middle)
    temp = []
    if cycle-1 in [middle-1, middle, middle+1]:
        temp.append("#")
    else:
        temp.append(".")
    return("#") if '#' in temp else ('.')


for entry in data:
    line = entry.strip("\n")
    line = line.split(" ")
   
    if line[0] == "noop":
        if cycle == 40:
         cycle = 0
        cycle +=1
    
        result.append(testOverlap(cycle, middle))


    elif line[0] == "addx":
        value = int(line[1])
        
        for j in range(2):
            if cycle == 40:
                cycle = 0
            cycle += 1
            result.append(testOverlap(cycle, middle))
        
        middle = middle + value


result = [x.replace("'", '') for x in result]

start = 0
for i in range(0,241,40):
    print(f"[{', '.join(result[start:i])}]")            #better readability code that removed strings. source: https://www.geeksforgeeks.org/python-ways-to-print-list-without-quotes/
    start = i


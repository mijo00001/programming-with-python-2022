
"""
Part1:
using BFS.

on reading some comments on AOC reddit, I found that BFS adn Dijkstra's were commonly used. A few also mentioned how changing the name of start and end to alphabets simplifies the code. Relied on this for overall idea: https://www.youtube.com/watch?v=deUdmffHZ0w&list=PLJt6nPUdQbiTXW_lHJ6d2u8NnsyPaKGeI&index=15&ab_channel=0xdf

Part2:
looping through all "a"s is very long. So, I implemented BFS backtracking 


ideas from https://www.youtube.com/watch?v=sBe_7Mzb47Y&t=173s&ab_channel=WilliamY.Feng, https://www.youtube.com/watch?v=xhe79JubaZI&ab_channel=hyper-neutrino, 

"""

# part1

with open("./day12.txt") as data:
    data = data.read().strip().split("\n")

grid = []

for i in data:
    temp = []
    for j in i:
        temp.append(j)
    grid.append(temp)


start = (0,0)
end = (0,0)
queued = []


for r, row in enumerate(grid):
    for c, val in enumerate(row):
        if val == 'S':
            start = (r,c)
            grid[r][c] = "a"
            queued.append((0, start))
        if val == "E":
            end = (r,c)
            grid[r][c] = "z"

traversed = [start]

signal = ""

while signal != "stop":
    step, (r, c) = queued.pop(0)
    


    for nr, nc in [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]:

        if nr < 0 or nc < 0:
            continue
        else:
            if nr >= len(grid) or nc >= len(grid[0]):
                continue
            else:
                if (nr, nc) in traversed:
                    continue
                else:
                    if ord(grid[nr][nc]) - ord(grid[r][c]) > 1:
                        continue

        if (nr, nc) == end:
            print(f'task1: {step +1}')
            signal = "stop"
            break                      
        traversed.append((nr, nc))
        queued.append((step + 1, (nr, nc)))



#  task2:



"""

looping through all "a"s is very long. So, I implemented BFS backtracking 

"""

grid = []

for i in data:
    temp = []
    for j in i:
        temp.append(j)
    grid.append(temp)


start = (0,0)
end = (0,0)
queued = []


for r, row in enumerate(grid):
    for c, val in enumerate(row):
        if val == 'S':
            start = (r,c)
            grid[r][c] = "a"
        if val == "E":
            end = (r,c)
            grid[r][c] = "z"
            queued.append((0, end))

traversed = [end]
Steps = []

while queued:
    step, (r, c) = queued.pop(0)
    


    for nr, nc in [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]:

        if nr < 0 or nc < 0:
            continue
        else:
            if nr >= len(grid) or nc >= len(grid[0]):
                continue
            else:
                if (nr, nc) in traversed:
                    continue
                else:
                    if ord(grid[nr][nc]) - ord(grid[r][c]) < -1:
                        continue
                    else:
                        if grid[nr][nc] == "a":
                            Steps.append(step+1)
                            break                      
                        traversed.append((nr, nc))
                        queued.append((step + 1, (nr, nc)))


print(f'task2: {min(Steps)}')
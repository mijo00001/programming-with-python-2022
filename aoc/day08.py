with open("./day08.txt") as file:
    data = file.read().split("\n")


rows = len(data)
cols = len(data[0])



count_ = 0
for row in range(1,rows-1):
    temp = ""
    for col in range(1, cols-1):
        Up = []
        Down= []
        Left = []
        Right = []
        temp  = temp + data[row][col]
        for up in range(row):
            Up.append(data[up][col])
        for down in range(row+1, rows):
            Down.append(data[down][col])
        for left in range(col):
            Left.append(data[row][left])
        for right in range(col+1, cols):
            Right.append(data[row][right])
        if int(max(Up)) < int(data[row][col]):
            count_ += 1
        elif max(Down) < data[row][col]:
            count_ += 1
        elif max(Left) < data[row][col]:
            count_ += 1
        elif max(Right) < data[row][col]:
            count_ += 1
        else:
            continue
        

print(f'task1: {count_ + (rows*2) + (cols*2) - 4}')

# task2



count = []
for row in range(1,rows-1):
    temp = 0
    for col in range(1, cols-1):
        Up = []
        Down= []
        Left = []
        Right = []
        for up in range(row):
            Up.append(data[up][col])
        for down in range(row+1, rows):
            Down.append(data[down][col])
        for left in range(col):
            Left.append(data[row][left])
        for right in range(col+1, cols):
            Right.append(data[row][right])
        c_up = 0
        c_down = 0
        c_left = 0
        c_right = 0
        for i in reversed(Up):
            if i < data[row][col]:
                c_up += 1
            elif i == data[row][col]:
                c_up += 1
                break
            else:
                break
        for i in Down:
            if i < data[row][col]:
                c_down += 1
            elif i == data[row][col]:
                c_down += 1
                break
            else:
                break
        for i in reversed(Left):
            if i < data[row][col]:
                c_left += 1
            elif i == data[row][col]:
                c_left += 1
                break
            else:
                break
        for i in Right:
            if i < data[row][col]:
                c_right += 1
            elif i >= data[row][col]:
                c_right += 1
                break
            else:
                break

        count.append(c_up * c_down * c_left * c_right)


print(f'task2: {max(count)}')
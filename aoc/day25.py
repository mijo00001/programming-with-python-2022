"""
 
 drew ideas from:
    - https://www.youtube.com/watch?v=s3mxIcr7fOQ&ab_channel=CodeSavant : to understand base changes
    - https://www.youtube.com/watch?v=MdLubTzLjIw&t=2s&ab_channel=hyper-neutrino, : as mentioned below, I kept getting an erroneous 4-digit output. I blindly inserted a piece of his code and it worked


"""

with open("./day25.txt") as data:
    data = data.read().strip().split()


toSANFU = {"0": 0, "1": 1, "2": 2, "-": -1,"=": -2,} 
toDecimal ={3: "=", 4: "-" }


def to_Snafu(input):
    value = 0
    base = 1
    reversed = input[::-1]
    for i in range(len(input)):
        value += base * toSANFU[reversed[i]]
        base *= 5
    return value


count = 0
for line in data:
    count += to_Snafu(line)

def to_Decimal(input):
    output = ""
    while input != 0:
        rem = input % 5
        input //= 5

        if rem <= 2:
            output = str(rem) + output
        else:
            output = toDecimal[rem] + output
            input += 1                                              #not sure why this is needed. I kept getting a 4-place answer instead of 5. I plugged this from neutrino's solution and it worked.
    return output


print(f'task1: {to_Decimal(count)}')



with open("./day09.txt") as file:
    data = file.read().split("\n")

# variables
HR, HC = 0, 0
TR, TC = 0, 0


def Hmove(direction, HR, HC):
    if direction == "R":
        HC += 1
    elif direction == "L":
        HC -= 1
    elif direction == "U":
        HR += 1
    elif direction == "D":
        HR -= 1
    else:
        print("error in directions")
    return (HR, HC)
    

        
def Tcheck(TR,TC, HR, HC, direction):
    TR,TC, HR, HC, direction = TR,TC, HR, HC, direction

    if not((abs(HR-TR) <= 1) and (abs(HC-TC) <= 1)):       #same position
        if direction == "R":
            TC += 1
            if HR-TR == 1:
                TR +=1
            elif HR-TR == -1:
                TR -=1
            else:
                pass
        elif direction == "L":
            TC -= 1
            if HR-TR == 1:
                TR +=1
            elif HR-TR == -1:
                TR -=1
            else:
                pass
        elif direction == "U":
            TR += 1
            if HC-TC == 1:
                TC +=1
            elif HC-TC == -1:
                TC -=1
            else:
                pass
        elif direction == "D":
            TR -= 1
            if HC-TC == 1:
                TC +=1
            elif HC-TC == -1:
                TC -=1
            else:
                pass
        else:
            print("error in TMove")
    else:
        pass

    return(TR, TC)
    


# read command and execute
track = []

for i in data:
    direction, steps = i.split(" ")
    for j in range(int(steps)):
        HR, HC = Hmove(direction, HR, HC)
        TR, TC = Tcheck(TR, TC, HR, HC, direction)
        track.append((TR,TC))

print(f'task1: {len(set(track))}')






